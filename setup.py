from setuptools import setup, find_packages
from os import path
from io import open

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='alice',
    version='1.0.0',
    description='Alice is Based on Customized Django Env',
    long_description=long_description,
    long_description_content_type='text/markdown',

    url='https://bitbucket.org/fotome/alice/src/master/',

    author='Yoshimasa Sugimori',
    author_email='developer@fabeee.co.jp',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
    ],
    keywords='django best practice',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['click', 'django'],
    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
    entry_points={
        'console_scripts': [
            'alice = alice.commands.main:main',
        ],
    }
)
