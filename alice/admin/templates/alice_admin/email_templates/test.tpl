{% extends "mail_templated/base.tpl" %}

{% block subject %}テスト用メール{% endblock %}

{% block body %}

テスト用のメールです。
お心当たりのない方は、お手数ですが
このまま破棄をお願い致します。

{% endblock %}
