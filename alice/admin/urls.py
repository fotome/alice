import re
from django.urls import path
from alice.core.utils import StringUtil
from alice.admin import controllers
from alice.admin.menus import is_model_menu, is_link_menu, get_admin_modules


def url_generator(admin_class):
    """モデルに対応するCRUD URLの精製
    """
    urls = []
    model_name = StringUtil.to_snake_case(admin_class.model.__name__)

    if admin_class.delete:
        urls.append(path(f'{model_name}/delete/', controllers.MultipleEditController.view, name=f'{model_name}/delete'))

    if admin_class.update:
        urls.append(path(f'{model_name}/edit/<int:pk>/', controllers.MultipleEditController.view, name=f'{model_name}/edit'))
        urls.append(path(f'{model_name}/update/<int:pk>/', controllers.MultipleUpdateController.view, name=f'{model_name}/update'))

    if admin_class.create:
        urls.append(path(f'{model_name}/create/', controllers.MultipleCreateController.view, name=f'{model_name}/create'))
        urls.append(path(f'{model_name}/regist/', controllers.MultipleRegistController.view, name=f'{model_name}/regist'))

    if admin_class.read:
        urls.append(path(f'{model_name}/<int:pk>/', controllers.MultipleGetController.view, name=f'{model_name}/detail'))
        urls.append(path(f'{model_name}/', controllers.MultipleListController.view, name=f'{model_name}/list'))

    return urls


def patterns_generator():
    """menuに対応するURLを作成
    """
    urls = [
        path('login/', controllers.Login.as_view(), name='login'),
        path('logout/', controllers.Logout.as_view(), name='logout'),
    ]

    admin_modules = get_admin_modules()
    for admin_module in admin_modules:
        for property in dir(admin_module):
            admin_class = getattr(admin_module, property)

            if is_model_menu(admin_class):
                urls += url_generator(admin_class)

            if hasattr(admin_module, 'urlpatterns'):
                urls += getattr(admin_module, 'urlpatterns')

    return urls + [path('', controllers.DashboardController.view, name='dashboard')]


app_name = 'alice_admin'
urlpatterns = patterns_generator()
