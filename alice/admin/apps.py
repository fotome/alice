from django.apps import AppConfig


class AliceAdminConfig(AppConfig):
    label = 'alice.admin'
    name = 'alice_admin'
