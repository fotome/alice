import datetime
from alice.core.services import BaseService


class MultipleModelService(BaseService):
    model = None

    def __init__(self, model=None):
        if model:
            self.model = model

    def create(self, data):
        model = self.model()
        for key, value in data.items():
            if hasattr(value, 'id') and hasattr(model, f'{key}_id'):
                setattr(model, f'{key}_id', value.id)
            if hasattr(model, key):
                setattr(model, key, value)
        return model.save()

    def update(self, pk, data):
        model = self.get(pk)
        for key, value in data.items():
            if hasattr(value, 'id') and hasattr(model, f'{key}_id'):
                setattr(model, f'{key}_id', value.id)
            if hasattr(model, key):
                setattr(model, key, value)
        return model.save()

    def delete(self, pk, hard=False):
        item = self.model.objects.get(pk=pk)
        if hard:
            item.hard_delete()
        else:
            item.delete()

    def get_column_list(self, exclude=['created_at', 'updated_at', 'deleted_at']):
        model = self.model()
        return {
          field.name: field
          for field in model._meta.get_fields()
          if field.name not in exclude
        }

    def get(self, pk):
        return self.model.objects.get(pk=pk)

    def get_all(self):
        items = self.model.objects.all().order_by('-updated_at')
        return items

    def get_item(self, pk, exclude=['created_at', 'updated_at', 'deleted_at']):
        model = self.get(pk)
        item = {
          field.name: getattr(model, field.name)
          for field in model._meta.get_fields()
          if field.name not in exclude and hasattr(model, field.name)
        }
        return item
