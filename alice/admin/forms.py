from django import forms
from django.conf.global_settings import DATETIME_INPUT_FORMATS
from alice.core.forms import LoginForm, BaseModelForm
from alice.admin.models import WysiwygField

if '%Y/%m/%d/ %H:%M' not in DATETIME_INPUT_FORMATS:
    DATETIME_INPUT_FORMATS += ('%Y/%m/%d/ %H:%M')

CONVERT_LIST = {
    'BigIntegerField': forms.IntegerField,
    'BinaryField': forms.CharField,
    'BooleanField': forms.BooleanField,
    'CharField': forms.CharField,
    'DateField': forms.DateField,
    'DateTimeField': forms.DateTimeField,
    'DecimalField': forms.DecimalField,
    'DurationField': forms.DurationField,
    'EmailField': forms.EmailField,
    'FileField': forms.FileField,
    'FilePathField': forms.FilePathField,
    'FloatField': forms.FloatField,
    'ForeignKey': forms.ModelChoiceField,
    'ImageField': forms.ImageField,
    'IntegerField': forms.IntegerField,
    'GenericIPAddressField': forms.GenericIPAddressField,
    'ManyToManyField': forms.ModelMultipleChoiceField,
    'NullBooleanField': forms.NullBooleanField,
    'PositiveIntegerField': forms.IntegerField,
    'PositiveSmallIntegerField': forms.IntegerField,
    'SlugField': forms.SlugField,
    'SmallIntegerField': forms.IntegerField,
    'TextField': forms.CharField,
    'TimeField': forms.TimeField,
    'URLField': forms.URLField,
    'UUIDField': forms.UUIDField,
    'WysiwygField': WysiwygField
}


class LoginForm(LoginForm):
    pass


def generate_meta(model, exclude):
    return type('Meta', (object, ), {
        'model': model.__class__,
        'exclude': exclude,
        'rules': []
    })


def generate_form(model, exclude=('created_at', 'deleted_at')):
    form_class_attrs = {
        'Meta': generate_meta(model, exclude)
    }
    form = forms.models.ModelFormMetaclass(
        f'{model.__class__.__name__}ModelForm',
        (BaseModelForm, ),
        form_class_attrs)
    return form


def get_fields_from(model):
    return {
        field.name: convert_field(field)
        for field in model._meta.get_fields()
        if type(field).__name__ in CONVERT_LIST.keys()
    }


def convert_field(model_field):
    REQUIRED_QUERYSET_FIELD = ['ForeignKey', 'ModelMultipleChoiceField', 'MultipleChoiceField']

    if type(model_field).__name__ in REQUIRED_QUERYSET_FIELD:
        return CONVERT_LIST[type(model_field).__name__](
            label=model_field.verbose_name,
            queryset=model_field.related_model.objects.all()
        )
    elif type(model_field).__name__ == 'WysiwygField':
        print(WysiwygField)
        return CONVERT_LIST[type(model_field).__name__](model_field.verbose_name)
    else:
        return CONVERT_LIST[type(model_field).__name__](
            label=model_field.verbose_name
        )
