from django import forms
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from alice.core.models import models, BaseModel
from alice.core.managers import UserManager


class BaseUserModel(AbstractBaseUser, PermissionsMixin, BaseModel):
    """ ベースユーザーモデル
    """
    email = models.EmailField('email address', unique=True)
    first_name = models.CharField('first name', max_length=30, blank=True, null=True)
    last_name = models.CharField('last name', max_length=150, blank=True, null=True)
    is_superuser = models.BooleanField('admin status', default=False, null=True)
    is_active = models.BooleanField('active', default=True, null=True)
    objects = UserManager()
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    @property
    def username(self):
        """ ユーザー名を返す

        Returns:
            str: メールアドレス
        """
        return self.email

    @property
    def fullname(self):
        """ フルネームを返す

        Returns:
            str: 姓(last_name) + 名(first_name)
        """
        if self.last_name and self.first_name:
            return f'{self.last_name} {self.first_name}'
        else:
            return f'{self.email}'

    def __str__(self):
        """ オブジェクトの文字列表現を返す

        Returns:
            str: メールアドレス
        """
        return self.fullname

    class Meta:
        abstract = True


class WysiwygField(models.TextField):

    class WysiwygField(forms.CharField):
        pass

    def formfield(self, **kwargs):
        return super().formfield(**{
            'form_class': self.WysiwygField,
            **kwargs,
        })
