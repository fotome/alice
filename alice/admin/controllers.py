from django.conf import settings
from django.shortcuts import redirect
from alice.core.utils import StringUtil
from alice.core.controllers import AuthRequiredController, BaseLoginView, BaseLogoutView
from alice.core.paginator import BasePaginator
from alice.admin.menus import get_admin_class
from alice.admin.forms import LoginForm, generate_form
from alice.admin.services import MultipleModelService

ADMIN_APP = settings.ADMIN_APP if hasattr(settings, 'ADMIN_APP') else 'alice_admin'


class BaseAdminController(AuthRequiredController):
    admin_class = None
    service = None

    @classmethod
    def redirect(cls, request):
        res = redirect(f'{ADMIN_APP}:login')
        res['location'] += '?next={}'.format(request.path)
        return res

    def __init__(self, request, **kwargs):
        super().__init__(request, **kwargs)
        self.set_admin_class()
        if self.admin_class:
            model = self.admin_class.model
            self.set_service(model)
            self.set_page_title(model)
            self.set_form(model)

    def set_service(self, model):
        self.service = MultipleModelService(model)
        self.context['model'] = model

    def set_admin_class(self):
        model_name = self.request.path.split('/')[2]
        self.admin_class = get_admin_class(StringUtil.to_upper_case(model_name))

    def set_page_title(self, model):
        self.context['page_title'] = model._meta.verbose_name_plural
        template = self.template.split('/')[-1]

        if template == 'list.html':
            self.context['page_title'] += '一覧'
        elif template == 'create.html':
            self.context['page_title'] += '新規作成'
        elif template == 'edit.html':
            self.context['page_title'] += '編集'
        elif template == 'detail.html':
            self.context['page_title'] += '詳細'

    def set_form(self, model):
        self.context['form'] = generate_form(model())

    def set_redirect_to(self, type):
        snake = StringUtil.to_snake_case(self.admin_class.model.__name__)
        self.redirect_to = f'{ADMIN_APP}:{snake}/{type}'


class MultipleListController(BaseAdminController):
    template = 'alice_admin/multiple_templates/list.html'
    method_only = ['GET']

    def get(self):
        page_no = self.request.GET.get('page', 1)
        items = self.service.get_all()
        self.context['paginator'] = BasePaginator(items, page_no)
        self.context['column_list'] = self.service.get_column_list(exclude=self.admin_class.exlude_field)


class MultipleGetController(BaseAdminController):
    template = 'alice_admin/multiple_templates/detail.html'
    method_only = ['GET']


class MultipleCreateController(BaseAdminController):
    template = 'alice_admin/multiple_templates/create.html'
    method_only = ['GET']

    def get(self):
        __old_input = self.get_old_input()
        if __old_input:
            self.context['form'] = self.context['form'](__old_input)


class MultipleRegistController(BaseAdminController):
    method_only = ['POST']

    def post(self):
        form = self.context['form'](self.request.POST)
        if not form.is_valid():
            self.redirect_back_with_input()
        else:
            self.service.create(form.cleaned_data)
            self.set_redirect_to('list')
            self.set_alert_success('新規レコードを追加しました。')


class MultipleEditController(BaseAdminController):
    template = 'alice_admin/multiple_templates/edit.html'
    method_only = ['GET']

    def get(self):
        self.context['pk'] = self.pk
        __old_input = self.get_old_input()
        if __old_input:
            self.context['form'] = self.context['form'](__old_input)
        else:
            data = self.service.get_item(self.pk)
            self.context['form'] = self.context['form'](data)


class MultipleUpdateController(BaseAdminController):
    method_only = ['POST']

    def post(self):
        form = self.context['form'](self.request.POST)
        if not form.is_valid():
            self.redirect_back_with_input()
        else:
            self.service.update(self.pk, form.cleaned_data)
            self.set_redirect_to('list')
            self.set_alert_success('対象のレコードを更新しました。')


class MultipleDeleteController(BaseAdminController):
    method_only = ['POST']

    def post(self):
        pk = self.request.POST.get('pk', None)
        if pk:
            self.service.delete(pk)
            self.set_redirect_to('list')
            self.set_alert_success('対象のレコードを削除しました。')


class BaseModelController(BaseAdminController):
    model = None
    service = None
    form = None
    exclude_field = ('created_at', 'deleted_at')
    template = 'alice_admin/multiple_templates/create.html'
    request_only = ['GET', 'POST']

    def __init__(self, request, **kwargs):
        super().__init__(request, **kwargs)
        self.set_service()
        self.set_form()
        self.set_page_title()

    def after(self):
        self.context['model'] = self.model
        self.context['exclude_field'] = self.exclude_field

    def set_service(self):
        if not self.service:
            super().set_service(self.model)

    def set_page_title(self):
        if 'page_title' not in self.context:
            super().set_page_title(self.model)

    def set_form(self):
        if not self.form:
            self.form = generate_form(self.model(), exclude=self.exclude_field)

    def set_redirect_to(self, type):
        snake = StringUtil.to_snake_case(self.model.__name__)
        self.redirect_to = f'{ADMIN_APP}:{snake}/{type}'


class BaseCreateModelController(BaseModelController):
    def get(self):
        __old_input = self.get_old_input()
        if __old_input:
            self.context['form'] = self.form(__old_input)
        else:
            self.context['form'] = self.form()

    def post(self):
        form = self.form(self.request.POST)
        if not form.is_valid():
            self.redirect_back_with_input()
        else:
            self.service.create(form.cleaned_data)
            self.set_redirect_to('list')
            self.set_alert_success('新規レコードを追加しました。')


class BaseEditModelController(BaseModelController):
    template = 'alice_admin/multiple_templates/edit.html'

    def get(self):
        self.context['pk'] = self.pk
        __old_input = self.get_old_input()
        if __old_input:
            self.context['form'] = self.form(__old_input)
        else:
            data = self.service.get_item(self.pk)
            self.context['form'] = self.form(data)
    
    def post(self):
        form = self.form(self.request.POST)
        if not form.is_valid():
            self.redirect_back_with_input()
        else:
            self.service.update(self.pk, form.cleaned_data)
            self.set_redirect_to('list')
            self.set_alert_success('対象のレコードを更新しました。')


class BaseDetailModelController(BaseModelController):
    template = 'alice_admin/multiple_templates/detail.html'
    request_only = ['GET']

    def get(self):
        self.context['item'] = self.service.get(self.pk)
        self.context['column_list'] = self.service.get_column_list(exclude=self.exclude_field)


class BaseListModelController(BaseModelController):
    request_only = ['GET']

    def get(self):
        page_no = self.request.GET.get('page', 1)
        items = self.service.get_all()
        self.context['paginator'] = BasePaginator(items, page_no)
        self.context['column_list'] = self.service.get_column_list(exclude=self.exclude_field)


class BaseDeleteModelController(BaseModelController):
    request_only = ['POST']

    def post(self):
        self.set_redirect_to('list')
        pk = self.request.POST.get('pk', None)
        if pk:
            self.service.delete(pk)
            self.set_alert_success('対象のレコードを削除しました。')
        else:
            self.set_alert_danger('対象のレコードが存在しませんでした。')


class DashboardController(BaseAdminController):
    template = 'alice_admin/dashboard.html'
    context = {'page_title': 'ダッシュボード'}
    method_only = ['GET', 'POST']


class Login(BaseLoginView):
    default_redirect_to = f'{ADMIN_APP}:dashboard'
    template_name = 'alice_admin/login.html'
    form_class = LoginForm
    extra_context = {
        'alice_site_title': 'ALICE Admin',
        'action_url': f'{ADMIN_APP}:login'
    }


class Logout(BaseLogoutView):
    next_page = f'{ADMIN_APP}:login'
