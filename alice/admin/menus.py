import os
import importlib
from django.conf import settings
ADMIN_MODULE_NAME = 'menus'


class ModelMenu:
    model = None
    exlude_field = ('created_at', 'updated_at', 'deleted_at')
    icon = None
    create = False
    read = False
    update = False
    delete = False

    def __init__(self):
        if not self.model:
            raise ValueError('model property is required.')


class LinkMenu:
    icon = None
    link = ''

    def __init__(self):
        if len(self.link) == 0:
            raise ValueError('link property is required.')


def has_admin_module(f):
    if os.path.isdir(os.path.join(settings.BASE_DIR, f)) and os.path.exists(os.path.join(settings.BASE_DIR, f, f'{ADMIN_MODULE_NAME}.py')):
        return True
    else:
        return False


def is_model_menu(target):
    if hasattr(target, 'model') and target.__name__ != ModelMenu.__name__:
        return True
    return False


def is_link_menu(target):
    if hasattr(target, 'link') and target.__name__ != LinkMenu.__name__:
        return True
    return False


def get_admin_modules():
    applications = [
        f for f in os.listdir(os.path.join(settings.BASE_DIR))
        if has_admin_module(f)
    ]
    return [
        importlib.import_module(f'{application}.{ADMIN_MODULE_NAME}')
        for application in applications
    ]


def get_admin_classes():
    admin_classes = []
    admin_modules = get_admin_modules()
    for admin_module in admin_modules:
        for module in dir(admin_module):
            admin_class = getattr(admin_module, module)
            if is_model_menu(admin_class) or is_link_menu(admin_class):
                admin_classes.append(admin_class())
    return admin_classes


def get_admin_class(model_name):
    admin_classes = get_admin_classes()
    for admin_class in admin_classes:
        if admin_class.model.__name__ == model_name:
            return admin_class
