var FADE_DURATION = 500;
var submitPrevent = true;

// フォーム送信時の確認処理
function confirmSubmit(form, e, msg) {
  if (submitPrevent) {
    e.preventDefault();
    if (confirm(msg)) {
      submitPrevent = false;
      form.submit();
    }
  }
}

$(function(){
  $('.confirm-regist').submit(function(e) {
    confirmSubmit($(this), e, '入力された内容でレコードを登録します。\nよろしいですか？');
  });
  $('.confirm-update').submit(function(e) {
    confirmSubmit($(this), e, '入力された内容でレコードを更新します。\nよろしいですか？');
  });
  $('.confirm-delete').submit(function(e) {
    confirmSubmit($(this), e, '対象のレコードを削除します。\nよろしいですか？');
  });
  $('html').fadeIn(FADE_DURATION);
});

window.addEventListener('beforeunload', function (e) {
  $('html').fadeOut(FADE_DURATION);
}, false)