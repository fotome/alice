from __future__ import absolute_import
from django.conf import settings
import os
from celery import Celery


environment = 'local' if settings.DEBUG else 'production'
os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'config.settings.{environment}')
app = Celery('config')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
