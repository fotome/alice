import os
import environ
from .common import *

env_file = os.path.join(BASE_DIR, '.env')
env = environ.Env()
env.read_env(env_file)

DEBUG = env('DEBUG')
ALLOWED_HOSTS = ['*']
DATABASES = {
    'default': env.db()
}
DATABASES['default']['ATOMIC_REQUESTS'] = True

# INTERNAL_IPS = type(str('c'), (), {'__contains__': lambda *a: True})()
# MIDDLEWARE += (
#     'debug_toolbar.middleware.DebugToolbarMiddleware',
# )
# INSTALLED_APPS += (
#     'debug_toolbar',
# )
# DEBUG_TOOLBAR_CONFIG = {
#     'INTERCEPT_REDIRECTS': False,
# }

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": env('CELERY_BROKER_URL'),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}
CELERY_BROKER_URL = env('CELERY_BROKER_URL')
CELERY_RESULT_BACKEND = env('CELERY_RESULT_BACKEND')

EMAIL_HOST = env('EMAIL_HOST')
EMAIL_PORT = env('EMAIL_PORT')
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = env('EMAIL_USE_TLS')

DEFAULT_FILE_STORAGE = env('DEFAULT_FILE_STORAGE')
MINIO_STORAGE_ENDPOINT = env('MINIO_STORAGE_ENDPOINT')
MINIO_STORAGE_ACCESS_KEY = env('MINIO_STORAGE_ACCESS_KEY')
MINIO_STORAGE_SECRET_KEY = env('MINIO_STORAGE_SECRET_KEY')
MINIO_STORAGE_USE_HTTPS = True if env('MINIO_STORAGE_USE_HTTPS') == 'True' else False
MINIO_STORAGE_AUTO_CREATE_MEDIA_BUCKET = env('MINIO_STORAGE_AUTO_CREATE_MEDIA_BUCKET')
MINIO_STORAGE_AUTO_CREATE_POLICY = True if env('MINIO_STORAGE_AUTO_CREATE_POLICY') == 'True' else False
MINIO_STORAGE_MEDIA_USE_PRESIGNED = True if env('MINIO_STORAGE_MEDIA_USE_PRESIGNED') == 'True' else False
MINIO_STORAGE_MEDIA_BACKUP_FORMAT = True if env('MINIO_STORAGE_MEDIA_BACKUP_FORMAT') == 'True' else False
MINIO_STORAGE_MEDIA_BACKUP_BUCKET = True if env('MINIO_STORAGE_MEDIA_BACKUP_BUCKET') == 'True' else False

"""
FIREBASE_JSON = env('FIREBASE_JSON')
FIREBASE_CONFIG = {
    'apiKey': env('FIREBASE_API_KEY'),
    'authDomain': env('FIREBASE_AUTH_DOMAIN'),
    'databaseURL': env('FIREBASE_DATABASE_URL'),
    'projectId': env('FIREBASE_PROJECT_ID'),
    'storageBucket': env('FIREBASE_STORAGE_BUCKET'),
    'messagingSenderId': env('FIREBASE_MESSAGING_SENDER_ID'),
    'appId': env('FIREBASE_APP_ID')
}
"""
