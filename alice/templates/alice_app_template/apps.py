from django.apps import AppConfig


class AppConfigName(AppConfig):
    name = 'app_name'