class BaseService:
    """
    ベースサービスクラス
    """
    model = None

    @classmethod
    def create(cls, data):
        for key, value in data.items():
            if hasattr(cls.model, key):
                setattr(cls.model, key, value)
        cls.model.save()

    @classmethod
    def get(cls, pk, with_deleted=False):
        """
        プライマリーキーで取得
        :pk プライマリーキー
        :with_deleted 削除済みレコードを含むかどうか
        """
        if with_deleted:
            return cls.model.all_objects.get(pk=pk)
        else:
            return cls.model.objects.get(pk=pk)

    @classmethod
    def get_all(cls, with_deleted=False):
        """
        全件取得
        :with_deleted 削除済みレコードを含むかどうか
        """
        if with_deleted:
            return cls.model.all_objects.all()
        else:
            return cls.model.objects.all()
