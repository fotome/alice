from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


class BasePaginator:
    def __init__(self, list, page_no, count=20):
        paginator = Paginator(list, count)
        try:
            page = paginator.page(page_no)
        except (EmptyPage, PageNotAnInteger):
            page = paginator.page(1)
        self.object = page
        self.items = page.object_list
