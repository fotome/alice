import base64
from django.contrib.auth import views as auth_views
from django.core import management
from django.core.exceptions import PermissionDenied
from django.http import (
    QueryDict,
    HttpResponseRedirect,
    HttpResponseForbidden,
    HttpResponseNotFound)
from django.http.response import JsonResponse
from django.utils.http import is_safe_url
from django.shortcuts import (
    render,
    redirect,
    resolve_url)
from alice.core.utils import AlertUtil


class BaseController:
    """ ベースコントローラ

    Attributes:
        template (str): HTMLテンプレート名
        redirect_to (str): リダイレクト先
        method_only (list): 許可するHTTPリクエストメソッド
        redirect_response (HttpResponseRedirect): リダイレクト先
    """
    template = ''
    redirect_to = ''
    method_only = []
    context = {}
    redirect_response = None

    def __init__(self, request, **kwargs):
        """ 初期処理

        Args:
            request (HttpReuest): HTTPリクエスト
            **kwargs (dict): キーワード引数
        """
        self.request = request
        for key in kwargs.keys():
            setattr(self, key, kwargs.get(key))
        self.pk = kwargs.get('pk', None)

    def response_forbidden(self):
        """ アクセス拒否

        Returns:
            HttpResponseForbidden:
        """
        return HttpResponseForbidden()

    def page_not_found(self):
        """ 存在しないページへのリクエスト

        Returns:
            HttpResponseNotFound:
        """
        return HttpResponseNotFound()

    def before(self):
        """ 前処理

        許可されていないHTTPリクエストメソッドの場合、アクセス拒否のレスポンスを返す

        Returns:
            HttpResponseForbidden
        """
        if len(self.method_only) > 0 and self.request.method not in self.method_only:
            return self.response_forbidden()

    def get(self):
        """ GET処理
        """
        pass

    def post(self):
        """ POST処理
        """
        pass

    def after(self):
        """ 後処理
        """
        pass

    def set_alert(self, type, text, strong=''):
        """ セッションにアラートをセットする

        Args:
            type (type): アラートタイプ
            text (str): アラートに表示するテキスト
            strong (str):
        """
        self.request.session[AlertUtil.TEMPLATE_ALERT_KEY] = {
            AlertUtil.TYPE_KEY: type,
            AlertUtil.TEXT_KEY: text,
            AlertUtil.STRONG_KEY: strong}

    def set_alert_success(self, text, strong=''):
        """ セッションに正常終了アラートを設定する

        Args:
            see method `set_alert`
        """
        self.set_alert(AlertUtil.SUCCESS, text, strong)

    def set_alert_info(self, text, strong=''):
        """ セッションに案内アラートを設定する

        Args:
            see method `set_alert`
        """
        self.set_alert(AlertUtil.INFO, text, strong)

    def set_alert_warning(self, text, strong=''):
        """ セッションに警告アラートを設定する

        Args:
            see method `set_alert`
        """
        self.set_alert(AlertUtil.WARNING, text, strong)

    def set_alert_danger(self, text, strong=''):
        """ セッションにエラーアラートを設定する

        Args:
            see method `set_alert`
        """
        self.set_alert(AlertUtil.ERROR, text, strong)

    def set_old_input(self, key='__old_input', encode='utf-8'):
        """ セッションに前回の入力内容を保存する

        Args:
            key (str): セッションキー
            encode (str): base64のエンコード
        """
        query_dict = self.request.POST.urlencode()
        self.request.session[key] = base64.b64encode(
            query_dict.encode(encode)).decode(encode)

    def redirect_back_with_input(self, key='__old_input', encode='utf-8'):
        """ 入力内容を保持したまま1つ前の画面にリダイレクトさせる

        Args:
            key (str): セッションキー
            encode (str): base64のエンコード
        """
        self.set_old_input(key=key, encode=encode)
        self.redirect_response = HttpResponseRedirect(
            self.request.META.get('HTTP_REFERER', '/'))

    def get_old_input(self, key='__old_input', encode='utf-8'):
        """ 前回の入力内容を取得する

        Args:
            key (str): セッションキー
            encode (str): base64のエンコード
        """
        if key in self.request.session.keys():
            query_dict = base64.b64decode(
                self.request.session[key]).decode(encode)
            del self.request.session[key]
            return QueryDict(query_dict, mutable=True)

    def response(self):
        if self.redirect_response is not None:
            return self.redirect_response
        elif not self.redirect_to:
            return render(self.request, self.template, self.context)
        else:
            return redirect(self.redirect_to)

    def json_response(self):
        """ コンテキストをjsonレスポンスにして返す

        Returns:
            JsonResponse: self.contextをjsonシリアライズしたオブジェクト
        """
        return JsonResponse(self.context)

    @classmethod
    def view(cls, request, **kwargs):
        """ BaseControllerを継承したクラスが共通で行う処理

        Args:
            request (HttpRequest): HTTPリクエスト
            **kwargs: キーワード引数

        Returns:
            HttpResponse
        """
        this = cls(request, **kwargs)
        res = this.before()
        if res is not None:
            return res
        else:
            if request.method == 'GET':
                this.get()
            elif request.method == 'POST':
                this.post()
            this.after()
            cls.context['HOME_URL'] = 'admin:user'
            return this.response()


class AuthRequiredController(BaseController):
    """ ベース認証必須コントローラ
    """
    LOGOUT_REDIRECT_URL = ''

    def __init__(self, request, **kwargs):
        super().__init__(request, **kwargs)
        self.allowed_permissions = kwargs.get('allowed_permissions', None)
        self.login_url = kwargs.get('login_url')

    def before(self):
        if len(self.method_only) > 0 and self.request.method not in self.method_only:
            return self.response_forbidden()
        if not self.request.user.is_authenticated or not self.custom_permission_check():
            raise PermissionDenied

    def custom_permission_check(self):
        result = True
        if self.allowed_permissions is not None:
            for flg in self.allowed_permissions:
                if not getattr(self.request.user, flg, False):
                    result = False
        return result

    @classmethod
    def view(cls, request, **kwargs):
        if request.user.is_authenticated:
            return super().view(request, **kwargs)
        else:
            return cls.redirect(request)

    @classmethod
    def redirect(cls, request):
        res = redirect(cls.LOGOUT_REDIRECT_URL)
        res['location'] += '?next={}'.format(request.path)
        return res


class BaseLoginView(auth_views.LoginView):
    """ ベースログインコントローラ
    """
    default_redirect_to = ''

    def redirect_override(self):
        return None

    def get_redirect_url(self):
        redirect_to = self.request.POST.get(
            self.redirect_field_name,
            self.request.GET.get(self.redirect_field_name, ''))

        url_is_safe = is_safe_url(
            url=redirect_to,
            allowed_hosts=self.get_success_url_allowed_hosts(),
            require_https=self.request.is_secure())

        if self.redirect_override() is not None:
            url_is_safe = False
            self.default_redirect_to = self.redirect_override()
        else:
            self.default_redirect_to = resolve_url(self.default_redirect_to)
        return redirect_to if url_is_safe else self.default_redirect_to


class BaseLogoutView(auth_views.LogoutView):
    """ ベースログアウトコントローラ
    """
    next_page = ''


class DefaultController(BaseController):
    template = 'alice_default/index.html'
    method_only = ['GET']


class AddAppController(BaseController):
    method_only = ['POST']

    def post(self):
        app_name = self.request.POST.get('application_name')
        management.call_command('add_app', app_name)
        self.context['msg'] = f'add application "{app_name}" success.'

    def response(self):
        return self.json_response()
