from threading import local
THREAD_LOCAL = local()


def set_thread_variable(key, value):
    setattr(THREAD_LOCAL, key, value)


def get_thread_variable(key, default=None):
    return getattr(THREAD_LOCAL, key, default)


def get_request():
    return get_thread_variable('request')
