from alice.core.middlewares.threadlocals import set_thread_variable
from django.utils.deprecation import MiddlewareMixin


class RequestStoreMiddleware(MiddlewareMixin):
    def process_request(self, reuqest):
        set_thread_variable('request', reuqest)
