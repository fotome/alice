import json
from django import template
from django.conf import settings


register = template.Library()
f = open(settings.PROJECT_CONF_JSON, 'r')
config_data = json.loads(f.read())

@register.simple_tag
def get_project_name():
    return config_data['PROJECT_NAME']

@register.simple_tag
def get_footer_text():
    return config_data['FOOTER_TEXT']


@register.simple_tag
def get_constants_by_key(key):
    return getattr(settings, key, '')


@register.simple_tag
def get_constants(*args):
    temp = None
    for arg in args:
        if arg in config_data:
            temp = config_data[arg]
        elif temp is not None and arg in temp:
            temp = temp[arg]

    return temp
