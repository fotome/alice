import os
from datetime import datetime
from django import template
from django.conf import settings
from django.contrib.staticfiles import finders
from django.template import loader, Context
from alice.core.utils import AlertUtil
register = template.Library()

@register.simple_tag
def show_alert(request):
    template = loader.get_template(settings.ALERT_TEMPLATE_PATH)
    context = {'show': False}
    if AlertUtil.TEMPLATE_ALERT_KEY in request.session:
        items = request.session[AlertUtil.TEMPLATE_ALERT_KEY].copy()
        del request.session[AlertUtil.TEMPLATE_ALERT_KEY]
        context = {
            'show': True,
            'type': items[AlertUtil.TYPE_KEY],
            'text': items[AlertUtil.TEXT_KEY],
            'strong': items[AlertUtil.STRONG_KEY]}
    return template.render(context)


@register.simple_tag
def static_with_date(path):
    try:
        time_stamp = os.stat(finders.find(path)).st_mtime
        date = datetime.fromtimestamp(time_stamp).strftime('%Y%m%d%H%M%S')
        file_url = os.path.join(settings.STATIC_URL, path)
        return file_url + f'?date={date}'
    except:
        return os.path.join(settings.STATIC_URL, path)

@register.simple_tag
def custom_static(path):
    if getattr(settings, 'USE_CDN', False):
        return os.path.join('https://alice-static.firebaseapp.com/static', path)
    else:
        return static_with_date(path)

@register.filter('to_int')
def to_int(value):
    try:
        value = int(value)
    except:
        pass
    return value

@register.filter('to_string')
def to_string(value):
    try:
        value = str(value)
    except:
        pass
    return value
