import os
from django import template
from django.conf import settings
from django.template import loader, Context
from django.urls import reverse, resolve
from django.utils.safestring import mark_safe
from alice.core.utils import StringUtil
from alice.core.middlewares.threadlocals import get_request
from alice.admin.menus import get_admin_classes
from alice.admin.urls import app_name
register = template.Library()


class AdminMenuNode(template.Node):
    def __init__(self, tag):
        self.tag = tag

    def render(self, context):
        return self.tag


class AdminScriptNode(template.Node):
    def __init__(self, tag):
        self.tag = tag

    def render(self, context):
        return self.tag


class AdminComponentrNode(template.Node):
    def __init__(self, template_name):
        self.template_name = template_name

    def render(self, context):
        t = context.template.engine.get_template(self.template_name)
        return t.render(Context({
            'request': get_request()
        }))


@register.filter()
def get_app_name():
    request = get_request()
    path = resolve(request.path).app_names[0]
    return path


@register.tag
def get_admin_menus(parser, token):
    tag = '<div class="menu_section">'
    tag += '<h3>管理メニュー</h3><ul class="nav side-menu">'
    for admin_class in get_admin_classes():
        tag += f'<li><a><i class="fa {admin_class.icon}"></i> {admin_class.model()._meta.verbose_name_plural} <span class="fa fa-chevron-down"></span></a><ul class="nav child_menu">'

        if admin_class.read:
            url = get_model_url(admin_class.model, 'list')
            tag += f'<li><a href="{url}">{admin_class.model()._meta.verbose_name_plural}一覧</a></li>'

        if admin_class.create:
            url = get_model_url(admin_class.model, 'create')
            tag += f'<li><a href="{url}">新規作成</a></li>'

        tag += '</ul></li>'
    tag += '</ul></div>'
    return AdminMenuNode(tag)


def get_admin_component(component_name):
    app_name = get_app_name()
    component = f'alice_admin/{component_name}.html'
    template_path = os.path.join(settings.BASE_DIR, app_name, 'templates')
    template_dirs = os.listdir(template_path)

    for template_dir in template_dirs:
        target_path = os.path.join(template_path, app_name, f'{component_name}.html')
        if os.path.exists(target_path):
            component = f'{app_name}/{component_name}.html'

    return component


@register.tag
def get_sidebar(parser, token):
    siderbar = get_admin_component('sidebar')
    return AdminComponentrNode(siderbar)


@register.tag
def get_header(parser, token):
    header = get_admin_component('header')
    return AdminComponentrNode(header)


@register.tag
def get_footer(parser, token):
    footer = get_admin_component('footer')
    return AdminComponentrNode(footer)


@register.tag
def get_profile(parser, token):
    profile = get_admin_component('profile')
    print(profile)
    return AdminComponentrNode(profile)


@register.simple_tag
def get_url(name):
    app_name = get_app_name()
    return reverse(f'{app_name}:{name}')


@register.simple_tag
def get_model_url(model, type, pk=None):
    app_name = get_app_name()
    if hasattr(model, '__name__'):
        snake = StringUtil.to_snake_case(model.__name__)
    else:
        snake = StringUtil.to_snake_case(model.__class__.__name__)
    if pk:
        return reverse(f'{app_name}:{snake}/{type}', args=[pk])
    else:
        return reverse(f'{app_name}:{snake}/{type}')


@register.filter()
def get_value_from(field, record, max_length=30):
    value = ''
    LONG_TEXT_FIELD = ['CharField', 'TextField', 'WysiwygField']

    if hasattr(record, field.name):
        value = getattr(record, field.name)
        value = value if value else ''
        type = field.__class__.__name__

        if type in LONG_TEXT_FIELD and len(value) > max_length:
            return str(value)[:max_length] + '…'

        elif type == 'IntegerField':
            for choice in field.choices:
                if value == choice[0]:
                    value = choice[1]
                    break

    return value


@register.filter()
def get_field_type(field):
    field_type = type(field.field).__name__
    if field_type == 'ModelChoiceField':
        template = loader.get_template('alice_admin/fields/base_select.html')
        context = {'field': field}
        return template.render(context)
    elif field_type == 'DateTimeField':
        template = loader.get_template('alice_admin/fields/base_datetime_input.html')
        context = {'field': field}
        return template.render(context)
    elif field_type == 'DateField':
        template = loader.get_template('alice_admin/fields/base_date.html')
        context = {'field': field}
        return template.render(context)
    elif field_type == 'WysiwygField':
        template = loader.get_template('alice_admin/fields/wysiwyg/form.html')
        context = {'field': field}
        return template.render(context)
    else:
        template = loader.get_template('alice_admin/fields/base_input.html')
        context = {'field': field}
        return template.render(context)


@register.filter(needs_autoescape=False)
def set_script(form):
    result = []
    for field in form:
      field_type = type(field.field).__name__
      if field_type == 'DateTimeField':
          result.append(f'<script>$("#{field.auto_id}").datetimepicker();</script>')
      if field_type == 'DateField':
          result.append(f'<script>$("#{field.auto_id}").datetimepicker(' + '{format: "YYYY-MM-DD"});</script>')
      elif field_type == 'WysiwygField':
          template = loader.get_template('alice_admin/fields/wysiwyg/tags.html')
          context = {'field': field}
          result.append(template.render(context))
    return mark_safe('\n'.join(result))
