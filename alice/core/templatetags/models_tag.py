import datetime, json
from django import template
from django.conf import settings
register = template.Library()

f = open(settings.PROJECT_CONF_JSON, 'r')
config_data = json.loads(f.read())

@register.simple_tag
def get_th_label(field_name):
    if 'TH_LABELS' in config_data and field_name in config_data['TH_LABELS']:
        return config_data['TH_LABELS'][field_name]
    else:
        return field_name

@register.simple_tag
def get_field_value(item, field, date_format=None):
    value = getattr(item, field, '')

    if isinstance(value, datetime.datetime):
        if 'TABLE_DATE_FORMAT' in config_data and field in config_data['TABLE_DATE_FORMAT']:
            value = value.strftime(config_data['TABLE_DATE_FORMAT'][field])
        else:
            value = value.strftime('%Y-%m-%d %H:%M:%S')

    elif isinstance(value, bool):
        if value:
            value = '<span class="label label-success">{}</span>'.format(value)
        else:
            value = '<span class="label label-danger">{}</span>'.format(value)

    return value
