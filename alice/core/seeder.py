class BaseSeeder:
    """
    BaseSeederクラス
    """
    model = None
    items = []
    truncate = True

    @classmethod
    def run(cls, cls_name):
        result = [cls.model.objects.create(**item) for item in cls.items]
        print(cls_name, 'Done.')
        return result
