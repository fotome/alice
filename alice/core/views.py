import base64
from django.conf import settings
from django.contrib.auth import views as auth_views
from django.core.exceptions import PermissionDenied
from django.http import QueryDict, HttpResponseRedirect, HttpResponseForbidden, HttpResponseNotFound
from django.http.response import JsonResponse
from django.utils.http import is_safe_url
from django.shortcuts import render, redirect, resolve_url
from .forms import LoginForm
from .utils import AlertUtil


class BaseView(object):
    """
    ベースビュークラス
    """
    template = ''
    redirect_to = ''
    method_only = []
    context = {}
    redirect_response = None

    def __init__(self, request, **kwargs):
        super().__init__()
        self.request = request
        for key in kwargs.keys():
            setattr(self, key, kwargs.get(key))
        self.pk = kwargs.get('pk', None)

    def response_forbidden(self):
        return HttpResponseForbidden()

    def page_not_found(self):
        return HttpResponseNotFound()

    def before(self):
        if len(self.method_only) > 0 and self.request.method not in self.method_only:
            return self.response_forbidden()

    def main(self):
        pass

    def after(self):
        pass

    def set_alert(self, type, text, strong=''):
        self.request.session[AlertUtil.TEMPLATE_ALERT_KEY] = {
            AlertUtil.TYPE_KEY: type,
            AlertUtil.TEXT_KEY: text,
            AlertUtil.STRONG_KEY: strong}

    def set_old_input(self, key='__old_input', encode='utf-8'):
        query_dict = self.request.POST.urlencode()
        self.request.session[key] = base64.b64encode(query_dict.encode(encode)).decode(encode)

    def redirect_back_with_input(self, key='__old_input', encode='utf-8'):
        self.set_old_input(key=key, encode=encode)
        self.redirect_response = HttpResponseRedirect(self.request.META.get('HTTP_REFERER', '/'))

    def get_old_input(self, key='__old_input', encode='utf-8'):
        if key in self.request.session.keys():
            query_dict = base64.b64decode(self.request.session[key]).decode(encode)
            del self.request.session[key]
            return QueryDict(query_dict, mutable=True)

    def response(self):
        if self.redirect_response is not None:
            return self.redirect_response
        elif not self.redirect_to:
            return render(self.request, self.template, self.context)
        else:
            return redirect(self.redirect_to)

    def json_response(self):
        return JsonResponse(self.context)

    @classmethod
    def view(cls, request, **kwargs):
        this = cls(request, **kwargs)
        res = this.before()
        if res is not None:
            return res
        else:
            this.main()
            this.after()
            return this.response()



class AuthRequiredView(BaseView):
    """
    ベース認証ビュークラス
    """
    def __init__(self, request, **kwargs):
        super().__init__(request, **kwargs)
        self.allowed_permissions = kwargs.get('allowed_permissions', None)
        self.login_url = kwargs.get('login_url')

    def before(self):
        if len(self.method_only) > 0 and self.request.method not in self.method_only:
            return self.response_forbidden()
        if not self.request.user.is_authenticated or not self.custom_permission_check():
            raise PermissionDenied

    def custom_permission_check(self):
        result = True
        if self.allowed_permissions is not None:
            for flg in self.allowed_permissions:
                if not getattr(self.request.user, flg, False):
                    result = False
        return result

    @classmethod
    def view(cls, request, **kwargs):
        if request.user.is_authenticated:
            return super().view(request, **kwargs)
        else:
            return cls.redirect(request)

    @classmethod
    def redirect(cls, request):
        res = redirect(settings.LOGOUT_REDIRECT_URL)
        res['location'] += '?next={}'.format(request.path)
        return res


class LoginView(auth_views.LoginView):
    """
    ベースログインビュークラス
    """
    default_redirect_to = ''
    template_name = 'alice_admin/login.html'
    form_class = LoginForm

    def redirect_override(self):
        return None

    def get_redirect_url(self):
        redirect_to = self.request.POST.get(
            self.redirect_field_name,
            self.request.GET.get(self.redirect_field_name, ''))

        url_is_safe = is_safe_url(
            url=redirect_to,
            allowed_hosts=self.get_success_url_allowed_hosts(),
            require_https=self.request.is_secure())

        if self.redirect_override() is not None:
            url_is_safe = False
            self.default_redirect_to = self.redirect_override()
        else:
            self.default_redirect_to = resolve_url(self.default_redirect_to)
        return redirect_to if url_is_safe else self.default_redirect_to


class LogoutView(auth_views.LogoutView):
    """
    ベースログアウトビュークラス
    """
    next_page = settings.LOGOUT_REDIRECT_URL
