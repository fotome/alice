import os
import re
import subprocess
import alice
from django.core.management.base import BaseCommand

TEMPLATES_DIR = os.path.join(alice.__path__[0], 'templates')
TEMPLATE_NAME = 'alice_app_template'
CMD_ADD_APP = 'cp -r {template_dir}/{template_name} {cwd}; mv {template_name} {app_name}'
MKDIR = 'mkdir {path}'
cwd = os.getcwd()


class Command(BaseCommand):

    app_name = ''

    def add_arguments(self, parser):
        parser.add_argument('app_name', nargs='+', type=str)

    def handle(self, *args, **options):
        self.app_name = options.get('app_name')[0]

        # 新規アプリケーションの作成
        self.copy_template()

        # アプリケーション最適化
        app_config_path = os.path.join(cwd, self.app_name, 'apps.py')
        self.optimize_file(app_config_path)

        app_urls_path = os.path.join(cwd, self.app_name, 'urls.py')
        self.optimize_file(app_urls_path)

        print(f'add new application {self.app_name}')

    def copy_template(self):
        template = os.path.join(TEMPLATES_DIR, TEMPLATE_NAME)
        res = subprocess.run(
            CMD_ADD_APP.format(
                template_dir=TEMPLATES_DIR,
                template_name=TEMPLATE_NAME,
                cwd=cwd,
                app_name=self.app_name),
            shell=True)
        path = os.path.join(cwd, self.app_name, 'templates', self.app_name)
        res = subprocess.run(MKDIR.format(path=path), shell=True)

        path = os.path.join(cwd, self.app_name, 'static', self.app_name)
        res = subprocess.run(MKDIR.format(path=path), shell=True)

    def optimize_file(self, file_path):
        temp = []
        with open(file_path, 'r') as f:
            lines = f.readlines()
            for line in lines:

                if 'AppConfigName' in line:
                    conf_name = re.sub("_(.)", lambda x: x.group(1).upper(), self.app_name)
                    conf_name = conf_name[0].upper() + conf_name[1:] + 'Config'
                    line = line.replace('AppConfigName', conf_name)

                if 'APP_NAME' in line:
                    line = line.replace('APP_NAME', self.app_name)
                temp.append(line)

        with open(file_path, 'w') as f:
            for line in temp:
                f.write(line)
