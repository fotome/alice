import os
import subprocess
import alice
from pathlib import Path
from django.core.management.base import BaseCommand

TEMPLATES_DIR = os.path.join(alice.__path__[0], 'templates')
TEMPLATE_NAME = 'alice_other_template/service.py'
CMD_ADD_COMMAND = 'cp {template_dir}/{template_name} {target_path}'
cwd = os.getcwd()


class Command(BaseCommand):
    """
    サービスモジュール追加コマンド
    """

    service_name = ''
    init_file_path = ''

    def add_arguments(self, parser):
        parser.add_argument('service_name', nargs='+', type=str)
        parser.add_argument('target_app', nargs='+', type=str)

    def handle(self, *args, **options):
        self.service_name = options.get('service_name')[0]
        self.target_app = options.get('target_app')[0]

        target_path = self.check_dir()

        if target_path:
            self.copy_template_file(target_path)
            print('create controller "{}" success.'.format(self.service_name))

    def check_dir(self):
        target_path = os.path.join(cwd, self.target_app)
        if not os.path.exists(target_path):
            print('error: cant find target application "{}"'.format(self.target_app))
            return False

        target_path = os.path.join(target_path, 'services')
        if not os.path.exists(target_path):
            os.mkdir(target_path)

        self.init_file_path = os.path.join(target_path, '__init__.py')

        return target_path

    def copy_template_file(self, target_path):
        target_path = os.path.join(target_path, self.service_name + '.py')
        cmd = CMD_ADD_COMMAND.format(
            template_dir=TEMPLATES_DIR, template_name=TEMPLATE_NAME, target_path=target_path)
        res = subprocess.run(cmd, shell=True)

        temp = []
        with open(target_path, 'r') as f:
            temp = f.readlines()
        with open(target_path, 'w') as f:
            for line in temp:
                if 'SERVICE_NAME' in line:
                    line = line.replace('SERVICE_NAME', self.service_name)
                f.write(line)

        from_path = '.'.join(os.path.join(target_path).replace('.py', '').split('/')[3:])
        with open(self.init_file_path, 'a') as f:
            print('\nfrom {} import {}'.format(from_path, self.service_name), file=f)
