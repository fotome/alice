import os
from django.core import management
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('app_name',nargs='+', type=str, help='targer application name.')
        parser.add_argument('--seeder', dest='seeder', default=None, help='target seeder name.')

    def handle(self, *args, **options):
        app_name = options.get('app_name')[0]
        seeder = options.get('seeder')

        seeds = os.path.join(app_name, 'seeds').replace('/', '.')
        module = __import__(seeds)

        if seeder is None:
            if hasattr(module.seeds, 'SEEDER_LIST') and len(getattr(module.seeds, 'SEEDER_LIST')) > 0:
                seeder_list = getattr(module.seeds, 'SEEDER_LIST')
            else:
                seeder_list = [cls_name for cls_name in list(dir(module.seeds)) if cls_name != 'BaseSeeder' and 'Seeder' in cls_name]

            for cls_name in seeder_list:
                self.run(module, cls_name)

            if hasattr(module.seeds, 'after'):
                module.seeds.after()
                print('after method Done.')

        else:
            self.run(module, seeder)

    def run(self, module, seeder):
        cls = getattr(module.seeds, seeder)
        instance = cls()
        if instance.truncate:
            cls.model.truncate()
        cls.model.set_foreign_key_checks(False)
        instance.run(seeder)
        cls.model.set_foreign_key_checks(True)
