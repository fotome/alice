import os
from django.core.management.base import BaseCommand
from alice.core.utils import StringUtil


class Command(BaseCommand):
    """
    SECRET_KEYの再生成コマンド
    """
    def handle(self, *args, **options):
        prefix = 'SECRET_KEY = '
        common_path = f'{os.getcwd()}/config/settings/common.py'
        secret_key = f'{prefix}\'{StringUtil.random_str(50, upper=False, symbol=True)}\''

        temp = []
        with open(common_path, 'r') as f:
            for line in f.readlines():
                if prefix in line:
                    temp.append(secret_key)
                else:
                    temp.append(line)

        with open(common_path + '.org', 'w') as f:
            for line in temp:
                f.write(line)
