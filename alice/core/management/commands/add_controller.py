import os
import subprocess
import alice
from django.core.management.base import BaseCommand

TEMPLATES_DIR = os.path.join(alice.__path__[0], 'templates')
TEMPLATE_NAME = 'alice_other_template/controller.py'
CMD_ADD_COMMAND = 'cp {template_dir}/{template_name} {target_path}'
cwd = os.getcwd()


class Command(BaseCommand):
    """
    コントローラーモジュール追加コマンド
    """

    controller_name = ''

    def add_arguments(self, parser):
        parser.add_argument('controller_name', nargs='+', type=str)
        parser.add_argument('target_app', nargs='+', type=str)

    def handle(self, *args, **options):
        self.controller_name = options.get('controller_name')[0]
        self.target_app = options.get('target_app')[0]

        target_path = self.check_dir()

        if target_path:
            self.copy_template_file(target_path)
            print('create controller "{}" success.'.format(self.controller_name))

    def check_dir(self):
        target_path = os.path.join(cwd, self.target_app)
        if not os.path.exists(target_path):
            print('error: cant find target application "{}"'.format(self.target_app))
            return False

        target_path = os.path.join(target_path, 'controllers')
        if not os.path.exists(target_path):
            os.mkdir(target_path)
    
        return target_path

    def copy_template_file(self, target_path):
        target_path = os.path.join(target_path, self.controller_name + '.py')
        cmd = CMD_ADD_COMMAND.format(template_dir=TEMPLATES_DIR, template_name=TEMPLATE_NAME, target_path=target_path)
        res = subprocess.run(cmd, shell=True)
