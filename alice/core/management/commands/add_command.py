import os
import subprocess
import alice
from django.core.management.base import BaseCommand

TEMPLATES_DIR = os.path.join(alice.__path__[0], 'templates')
TEMPLATE_NAME = 'alice_other_template/command.py'
CMD_ADD_COMMAND = 'cp {template_dir}/{template_name} {target_path}'
cwd = os.getcwd()

class Command(BaseCommand):

    command_name = ''

    def add_arguments(self, parser):
        parser.add_argument('command_name', nargs='+', type=str)
        parser.add_argument('target_app', nargs='+', type=str)

    def handle(self, *args, **options):
        self.command_name = options.get('command_name')[0]
        self.target_app = options.get('target_app')[0]

        target_path = self.check_dir()
        if target_path:
            target_path = os.path.join(target_path, self.command_name + '.py')
            cmd = CMD_ADD_COMMAND.format(template_dir=TEMPLATES_DIR, template_name=TEMPLATE_NAME, target_path=target_path)
            res = subprocess.run(cmd, shell=True)
            print('create command "{}" success.'.format(self.command_name))

    def check_dir(self):
        target_path = os.path.join(cwd, self.target_app)
        if not os.path.exists(target_path):
            print('error: cant find target application "{}"'.format(self.target_app))
            return False

        target_path = os.path.join(target_path, 'management')
        if not os.path.exists(target_path):
            os.mkdir(target_path)

        target_path = os.path.join(target_path, 'commands')
        if not os.path.exists(target_path):
            os.mkdir(target_path)

        return target_path
