from django.core import management
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('target_app', nargs='+', type=str)
        parser.add_argument('name_space', nargs='*', type=str)

    def handle(self, *args, **options):
        self.target_app = options.get('target_app')[0] if len(options.get('target_app')) > 0 else None
        self.name_space = options.get('name_space')[0] if len(options.get('name_space')) > 0 else None

        management.call_command('generate_footer', self.target_app, self.name_space)
        management.call_command('generate_header', self.target_app, self.name_space)
        management.call_command('generate_profile', self.target_app, self.name_space)
        management.call_command('generate_sidebar', self.target_app, self.name_space)