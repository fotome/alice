import os, subprocess
from django.core import management
from django.core.management.base import BaseCommand
from django.conf import settings
import alice.core


class Command(BaseCommand):
    def add_arguments(self, parser):
         parser.add_argument('--ip', dest='ip', default='0.0.0.0', help='ip address.')
         parser.add_argument('--port', dest='port', default='8000', help='port number.')
         parser.add_argument('--ssl', dest='ssl', default=False, help='ssl enable.')
         parser.add_argument('--celery', dest='celery', default=False, help='celery enable.')

    def handle(self, *args, **options):
        ip = options.get('ip')
        port = options.get('port')
        ssl = options.get('ssl')
        celery = options.get('celery')

        if celery:
            celery_cmd = f'celery multi restart local -A config.settings -l info --logfile=logs/%n%I.log --pidfile=logs/%n.pid --concurrency=1'
            subprocess.run(f'rm -f logs/local*', shell=True, cwd=f'/src')
            subprocess.run(celery_cmd, shell=True, cwd=f'/src')

        if ssl:
            ssl_path = os.path.join(alice.core.__path__[0], 'ssl')
            crt_path = os.path.join(ssl_path, 'server.crt')
            key_path = os.path.join(ssl_path, 'server.key')
            management.call_command('runsslserver', f'{ip}:{port}', '--certificate', crt_path, '--key', key_path, '--settings=config.settings.local')
        else:
            management.call_command('runserver', f'{ip}:{port}', '--settings=config.settings.local')
