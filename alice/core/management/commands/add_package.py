import subprocess
from django.core import management
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    """
    フォームモジュール追加コマンド
    """

    package_name = ''

    def add_arguments(self, parser):
        parser.add_argument('package_name', nargs='+', type=str)
        parser.add_argument('target_app', nargs='+', type=str)

    def handle(self, *args, **options):
        self.package_name = options.get('package_name')[0]
        self.target_app = options.get('target_app')[0]
        
        ### コントローラー作成
        check = input('Need to add controller file ? [y/N] >  ').strip()
        while check.lower() != 'y' and check.lower() != 'n':
            print('plz input "y" or "N".')
            check = input('Need to add controller file ? [y/N] >  ').strip()
        if check == 'y':
            management.call_command(
                'add_controller',
                f'{self.package_name}Controller',
                self.target_app)

        ### フォーム作成
        check = input('Need to add forms file ? [y/N] >  ').strip()
        while check.lower() != 'y' and check.lower() != 'n':
            print('plz input "y" or "N".')
            check = input('Need to add forms file ? [y/N] >  ').strip()
        if check == 'y':
            management.call_command(
                'add_forms',
                f'{self.package_name}Forms',
                self.target_app)

        ### サービス作成
        check = input('Need to add service file ? [y/N] >  ').strip()
        while check.lower() != 'y' and check.lower() != 'n':
            print('plz input "y" or "N".')
            check = input('Need to add service file ? [y/N] >  ').strip()
        if check == 'y':
            management.call_command(
                'add_service',
                f'{self.package_name}Service',
                self.target_app)

        print('done.')
        
