import os
import subprocess
import alice
from django.core.management.base import BaseCommand

TEMPLATES_DIR = alice.__path__[0]
TEMPLATE_NAME = 'admin/templates/alice_admin/profile.html'
CMD_ADD_COMMAND = 'cp {template_dir}/{template_name} {target_path}'
cwd = os.getcwd()


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('target_app', nargs='+', type=str)
        parser.add_argument('name_space', nargs='*', type=str)

    def handle(self, *args, **options):
        self.target_app = options.get('target_app')[0] if len(options.get('target_app')) > 0 else None
        self.name_space = options.get('name_space')[0] if len(options.get('name_space')) > 0 else None

        if not self.name_space:
            self.name_space = self.target_app
            target_path = self.check_dir()

            if target_path:
                self.copy_profile_file(target_path)
                print('generate profile success.')

    def check_dir(self):
        target_path = os.path.join(cwd, self.target_app)
        if not os.path.exists(target_path):
            print('error: cant find target application "{}"'.format(self.target_app))
            return False

        target_path = os.path.join(target_path, 'templates', self.name_space)
        if not os.path.exists(target_path):
            os.makedirs(target_path)

        return target_path

    def copy_profile_file(self, target_path):
        target_path = os.path.join(target_path, 'profile.html')
        cmd = CMD_ADD_COMMAND.format(template_dir=TEMPLATES_DIR, template_name=TEMPLATE_NAME, target_path=target_path)
        res = subprocess.run(cmd, shell=True)
