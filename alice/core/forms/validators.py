from django.core.validators import RegexValidator


def multiple_validator(lower=True, uppper=True, numeric=True, sybols=[], message='invalid'):
    regex = []
    if lower:
        regex.append('a-z')
    if uppper:
        regex.append('A-Z')
    if numeric:
        regex.append('0-9')

    regex += [f'\{s}' for s in sybols]
    regex = '^[' + ''.join(regex) + ']*$'

    return RegexValidator(regex, message)
