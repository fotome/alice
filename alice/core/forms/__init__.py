from django import forms
from django.forms.utils import ErrorList
from django.contrib.auth.forms import AuthenticationForm


class BaseForm(forms.Form):
    """
    BaseFormクラス
    """
    def clean(self):
        cleaned_data = super().clean()
        self.__custom_validate(cleaned_data)
        return cleaned_data

    def __custom_validate(self, cleaned_data):
        for rule in self.Meta.rules:
            print(rule)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs["class"] = "form-control"

    class Meta:
        rules = []


class BaseModelForm(forms.ModelForm):
    """
    BaseModelFormクラス
    """
    def clean(self):
        cleaned_data = super().clean()
        self.__custom_validate(cleaned_data)
        return cleaned_data

    def __custom_validate(self, cleaned_data):
        for rule in self.Meta.rules:
            print(rule)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs["class"] = "form-control"


class LoginForm(AuthenticationForm):
    """
    LoginFormクラス
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'
            field.widget.attrs['placeholder'] = field.label
