"""
### ABC ###
Alice BaseModel & QuerySet Classes
"""
from datetime import datetime
from django import forms
from django.conf import settings
from django.core.cache import cache
from django.db import models, connections
from alice.core.managers import SoftDeletionManager


class BaseModel(models.Model):
    """
    Alice BaseModel Class
    """
    created_at = models.DateTimeField('created at', default=datetime.now)
    updated_at = models.DateTimeField('updated at', auto_now=True)
    deleted_at = models.DateTimeField('deleted at',blank=True, null=True)

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    @classmethod
    def get_connection(cls, db=None):
        if db is not None:
            con = connections[db]
        else:
            con = connections['default']
        return con

    @classmethod
    def set_foreign_key_checks(cls, bool, db=None):
        with cls.get_connection(db).cursor() as cursor:
            cursor.execute("SET FOREIGN_KEY_CHECKS = {};".format(str(int(bool))))

    @classmethod
    def truncate(cls, db=None):
        cls.set_foreign_key_checks(False, db)
        with cls.get_connection(db).cursor() as cursor:
            cursor.execute('TRUNCATE TABLE {0}'.format(cls._meta.db_table))
        cls.set_foreign_key_checks(False, db)

    @property
    def is_alive(self):
        return True if self.deleted_at is None else False

    def alive(self):
        self.deleted_at = None
        self.save()

    def delete(self):
        self.deleted_at = datetime.now()
        self.save()

    def hard_delete(self):
        super().delete()

    class Meta:
        abstract = True


class BaseCacheModel(BaseModel):
    """
    Alice BaseCacheModel Class
    """
    def delete(self):
        cache.delete(self.get_cache_path(self.id))
        cache.delete(self.get_all_cache_path())

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        cache.delete(self.get_cache_path(self.id))
        cache.delete(self.get_all_cache_path())
        cache.set(self.get_cache_path(self.id), self, getattr(settings, 'CACHE_DEFAULT_AGE', 60 * 60 * 24))

    @classmethod
    def get_cache_path(cls, pk):
        if hasattr(cls, '__name__'):
            return f'{cls.__name__}/{pk}/'
        else:
            return f'{cls.__class__.__name__}/{pk}/'

    @classmethod
    def get_list_cache_path(cls, pk_list):
        pk_list = [str(pk) for pk in pk_list]
        pk_list = '-'.join(pk_list)
        if hasattr(cls, '__name__'):
            return f'{cls.__name__}/list-{pk_list}/'
        else:
            return f'{cls.__class__.__name__}/list-{pk_list}/'

    @classmethod
    def get_all_cache_path(cls):
        if hasattr(cls, '__name__'):
            return f'{cls.__name__}/all/'
        else:
            return f'{cls.__class__.__name__}/all/'

    @classmethod
    def get_all_choices_cache_path(cls):
        if hasattr(cls, '__name__'):
            return f'{cls.__name__}/all_choices/'
        else:
            return f'{cls.__class__.__name__}/all_choices/'

    @classmethod
    def get_all(cls):
        cache_path = cls.get_all_cache_path()
        models = cache.get(cache_path, None)
        if models is None:
            models = cls.objects.all().select_related_all()
            cache.set(cache_path, models, getattr(settings, 'CACHE_DEFAULT_AGE', 60 * 60 * 24))
        else:
            models = models
        return models

    @classmethod
    def get_all_choices(cls):
        cache_path = cls.get_all_choices_cache_path()
        choices = cache.get(cache_path, None)
        if choices is None:
            choices = [(data.id, data.name) for data in cls.get_all()]
            cache.set(cache_path, choices, getattr(settings, 'CACHE_DEFAULT_AGE', 60 * 60 * 24))
        return choices

    @classmethod
    def get(cls,pk):
        cache_path = cls.get_cache_path(pk)
        model = cache.get(cache_path, None)
        if model is None:
            try:
                model = cls.objects.select_related_all().get(pk=pk)
                cache.set(cache_path, model, getattr(settings, 'CACHE_DEFAULT_AGE', 60 * 60 * 24))
            except cls.DoesNotExist:
                model = None
        return model

    @classmethod
    def get_list_by_id(cls, pk_list):
        cache_path = cls.get_list_cache_path(pk_list)
        model_list = cache.get(cache_path, None)
        if model_list is None:
            model_list = cls.objects.select_related_all().filter(id__in=pk_list)
            cache.set(cache_path, model_list, getattr(settings, 'CACHE_DEFAULT_AGE', 60 * 60 * 24))
        return model_list

    class Meta:
        abstract = True


class AlicePackage(BaseModel):
    package_name = models.CharField('package name', max_length=300, blank=True, null=True)
    use = models.CharField('use', max_length=300, blank=True, null=True)
    is_active = models.BooleanField('active', default=True, null=True)

    class Meta:
        db_table = 'alice_packages'
        verbose_name = verbose_name_plural = 'ALICEパッケージ'
