from django.urls import path
from django.conf import settings
from alice.core.controllers import DefaultController, AddAppController


if settings.DEBUG:
    urlpatterns = [
        path('add_app/', AddAppController.view),
        path('', DefaultController.view),
    ]
