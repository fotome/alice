from django.apps import AppConfig


class AliceCoreConfig(AppConfig):
    label = 'alice.core'
    name = 'alice_core'
