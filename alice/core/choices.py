from djchoices import DjangoChoices, ChoiceItem


class BaseChoices(DjangoChoices):
    @classmethod
    def get_item(cls, value):
        attribute_for_value = cls.attributes[int(value)]
        return cls._fields[attribute_for_value]

    @classmethod
    def get_item_by_value_list(cls, value_list):
        return [cls.get_item(value) for value in value_list]

    @classmethod
    def get_label(cls, value):
        return cls.get_item(value).label

    @classmethod
    def get_value_from_label(cls, label):
        value = [i for i in cls.values if cls.values[i] == label]
        if len(value) > 0:
            return value[0]
        else:
            return None
