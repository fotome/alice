from django.conf import settings
from mail_templated import send_mail


class BaseMailer:
    """
    ベースメーラークラス
    """
    template = ''
    send_from = ''
    auth_user = ''
    auth_password = ''
    params = {}
    send_to = []

    def __init__(self, params={}, send_to=[], send_from=None):
        super().__init__()
        self.params = params
        self.send_to = send_to
        if len(self.send_from) == 0:
            self.send_from = settings.EMAIL_FROM if send_from is None else send_from

    def add_send_to(self, target):
        self.send_to.append(target)

    def send(self):
        if len(self.auth_user) > 0 and len(self.auth_password) > 0:
            send_mail(self.template, self.params, self.send_from,
                      list(set(self.send_to)), auth_user=self.auth_user, auth_password=self.auth_password)
        else:
            send_mail(self.template, self.params, self.send_from, list(set(self.send_to)))
