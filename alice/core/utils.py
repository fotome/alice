import os
import random
import hashlib
import re
from django.urls import reverse
from mail_templated import send_mail
from django.conf import settings
from minio_storage.storage import create_minio_client_from_settings, MinioStorage


class AlertUtil:
    """
    AlertUtilクラス
    """
    TEMPLATE_ALERT_KEY = 'template_alert'
    TYPE_KEY = 'template_alert_type'
    TEXT_KEY = 'template_alert_text'
    STRONG_KEY = 'template_alert_strong'

    SUCCESS = 'success'
    INFO = 'info'
    WARNING = 'warning'
    ERROR = 'danger'


class StringUtil:
    """
    StringUtilクラス
    """
    @classmethod
    def random_str(cls, n=100, lower=True, upper=True, digit=True, symbol=False):
        letters = ''
        if lower:
            letters += 'abcdefghijklmnopqrstuvwxyz'
        if upper:
            letters += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        if digit:
            letters += '0123456789'
        if symbol:
            letters += '!@#$%^&*(-_=+)'
        return ''.join((random.SystemRandom().choice(letters) for i in range(n)))

    @classmethod
    def hash_md5(cls, text):
        return hashlib.md5(text.encode()).hexdigest()

    @classmethod
    def to_snake_case(cls, text):
        snake = re.sub('([A-Z])', lambda x: "_" + x.group(1).lower(), text)
        return snake.lower()[1:]

    @classmethod
    def to_upper_case(cls, text):
        def initial_upper(text):
            return text[0].upper() + text[1:] if len(text) > 0 else ''
        return ''.join([initial_upper(t) for t in text.split('_')])


class UrlUtil:
    """
    UrlUtilクラス
    """
    @classmethod
    def get_full_url(cls, request, path, query=None, kwargs={}):
        scheme = request.scheme
        host = request.META['HTTP_HOST']

        return cls.create_full_url(scheme, host, path, query=query, kwargs=kwargs)

    @classmethod
    def create_full_url(cls, scheme, host, path, query=None, kwargs={}):
        path = reverse(path, kwargs=kwargs)
        full_url =  f'{scheme}://{host}{path}'

        if query is not None:
            query_str = [k+'='+v for k, v in query.items()]
            full_url = full_url + '?' + '&'.join(query_str)
        return full_url


class BaseMinioUtil(MinioStorage):
    """
    （S3用）BaseMinioUtilクラス
    """
    prefix = None
    bucket_name = None

    def __init__(self, bucket_name=None):
        self.client = create_minio_client_from_settings()
        if bucket_name is not None:
            self.bucket_name = bucket_name
        if self.bucket_name is None:
            self.bucket_name = settings.MINIO_STORAGE_MEDIA_BUCKET_NAME
        if settings.MINIO_STORAGE_AUTO_CREATE_MEDIA_BUCKET:
            self.create_bucket_if_not_exists(self.bucket_name)
        if settings.MINIO_STORAGE_AUTO_CREATE_POLICY:
            self.client.set_bucket_policy(self.bucket_name, self._policy("READ_WRITE"))

        is_ssl = settings.MINIO_STORAGE_USE_HTTPS
        self.base_url = f"{'https' if is_ssl else 'http'}://{settings.MINIO_STORAGE_ENDPOINT}/{self.bucket_name}"

        super().__init__(
            self.client, self.bucket_name, base_url=self.base_url,
            auto_create_bucket=settings.MINIO_STORAGE_AUTO_CREATE_MEDIA_BUCKET,
            auto_create_policy=settings.MINIO_STORAGE_AUTO_CREATE_POLICY,
            presign_urls=settings.MINIO_STORAGE_MEDIA_USE_PRESIGNED,
            backup_format=settings.MINIO_STORAGE_MEDIA_BACKUP_FORMAT,
            backup_bucket=settings.MINIO_STORAGE_MEDIA_BACKUP_BUCKET)

    def create_bucket_if_not_exists(self, bucket_name, region=None):
        if not self.client.bucket_exists(bucket_name):
            self.client.make_bucket(bucket_name, location=region)

    def put(self, file):
        name, ext = os.path.splitext(file.name)
        name = f"{StringUtil.random_str()}{ext}"
        if self.prefix is not None:
            name = os.path.join(self.prefix, name)
        super().save(name, file)
        return f'{self.bucket_name}/{name}'

    def put_file(self, path):
        ext = path.split('.')[-1]
        name = f"{StringUtil.random_str()}.{ext}"

        bucket_name = self.bucket_name
        if self.prefix is not None:
            name = self.prefix + '/' + name

        self.client.fput_object(bucket_name, name, path)
        return f'{bucket_name}/{name}'

    def remove(self, name):
        self.client.remove_object(self.bucket_name, name)

    def save(self, key, request_files):
        name = ''
        files = request_files.getlist(key, [])
        for file in files:
            name, ext = os.path.splitext(file.name)
            name = f"{StringUtil.random_str()}{ext}"
            if self.prefix is not None:
                name = os.path.join(self.prefix, name)
            super().save(name, file)
        return f'{self.bucket_name}/{name}'
