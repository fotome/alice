import os
import subprocess

DEFAULT_PYTHON_VERSION = '3.7.7'
TEMPLATES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../', 'templates')
CREATE_ALICE_PROJECT = 'cp -r {template} {cwd}; mv alice_project_template {project_name}'
SET_PYENV = 'pyenv virtualenv {version} {env_name}; cd {project_path}; pyenv local {env_name}'
SET_GIT_REPO = 'cd {project_path};git init;git remote add origin {git};git checkout -b develop;git add .;git commit -m "initial commit";git push -u origin develop'
cwd = os.getcwd()


def create_django_project(project_name):
    """
    djangoのプロジェクトを作成
    """
    template = os.path.join(TEMPLATES_DIR, 'alice_project_template')
    res = subprocess.run(
        CREATE_ALICE_PROJECT.format(
            template=template,
            cwd=cwd,
            project_name=project_name),
        shell=True)


def set_docker_file(project_name, version):
    """
    docker-compose.yml編集
    """
    project_path = os.path.join(cwd, project_name)
    with open(os.path.join(project_path, 'docker-compose.yml'), 'r') as f:
        data = f.read()
    data = data.replace('{project_name}', project_name)
    with open(os.path.join(project_path, 'docker-compose.yml'), 'w') as f:
        f.write(data)

    with open(os.path.join(project_path, 'Dockerfile'), 'r') as f:
        data = f.read()
    data = data.replace('{project_name}', project_name)
    data = data.replace('{version}', version if version else DEFAULT_PYTHON_VERSION)
    with open(os.path.join(project_path, 'Dockerfile'), 'w') as f:
        f.write(data)


def set_pyenv(project_name, version, env_name=None):
    """
    pyenvの適応
    """
    project_path = os.path.join(cwd, project_name)

    if env_name is None:
        env_name = project_name

    res = subprocess.run(
        SET_PYENV.format(project_path=project_path, version=version, env_name=env_name),
        shell=True)


def set_git_repo(project_name, git):
    """
    git repoの適応
    """
    project_path = os.path.join(cwd, project_name)
    res = subprocess.run(SET_GIT_REPO.format(project_path=project_path, git=git), shell=True)
