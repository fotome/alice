import os
import subprocess


DOCKER_START = 'docker-compose up -d;'
DOCKER_LOG = 'docker logs web -f;'
DOCKER_STOP = 'docker-compose down;'
DOCKER_BASH = 'docker exec -it web bash;'
cwd = os.getcwd()


def log():
    """
    dockerのログを表示
    """
    res = subprocess.run(DOCKER_LOG, shell=True)

def start(daemon=None):
    """
    docker-compose up
    """
    res = subprocess.run(DOCKER_START, shell=True)
    if daemon is None:
        log()

def stop():
    """
    docker-compose down
    """
    res = subprocess.run(DOCKER_STOP, shell=True)

def restart(daemon=None):
    """
    docker restart
    """
    stop()
    start(daemon=daemon)

def web_bash():
    """
    bash on docker web
    """
    res = subprocess.run(DOCKER_BASH, shell=True)
