import click
from alice.commands.services import create, docker

@click.group()
def main():
    pass

@main.command()
@click.argument('project_name')
@click.option('--git', '-g', default=None, help='git repo url')
@click.option('--python', '-p', default=None, help='pyenv python version')
@click.option('--pyenv', '-e', default=None, help='pyenv version name')
def create_project(project_name, git=None, python=None, pyenv=None):
    print('creating new django project...')
    create.create_django_project(project_name)

    print('optimizing files...')
    create.set_docker_file(project_name, version=python)

    if python is not None:
        print('set pyenv...')
        create.set_pyenv(project_name, python, pyenv)

    if git is not None:
        print('set git repo...')
        create.set_git_repo(project_name, git)

    print('done.')

@main.command()
@click.option('--daemon', '-d', default=None, help='daemon')
def docker_start(daemon=None):
    docker.start(daemon=daemon)

@main.command()
def docker_stop():
    docker.stop()

@main.command()
@click.option('--daemon', '-d', default=None, help='daemon')
def docker_restart(daemon=None):
    docker.restart(daemon=daemon)

@main.command()
def docker_log():
    docker.log()

@main.command()
def docker_bash():
    docker.web_bash()
