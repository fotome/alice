# A Base Custom Django Env
新規プロジェクト用カスタムDjangoプロジェクト作成コマンド

![　](https://i.imgur.com/EIqcrgP.png =250x250)


## Install
プロジェクトをダウンロード後、ルートディレクトリで

```
pip install -e .
```

を実行してください。


## How to Use
pyenvがインストールされている場合は
`-p`, `-e`のオプションでpyenvの環境を作成・適応できます。

```
alice create_project {project_name} -p {version} -e {env_name} -g {git repo url}
```
※プロジェクトの初期化が必要です。
```
cd {project_name}
pip install -r requirements.txt
pip install git+https://dev-fotome@bitbucket.org/fotome/alice.git
```

開発環境立ち上げ
```
alice docker_start
```

## Document
https://fotome.atlassian.net/wiki/spaces/ALICE/overview
